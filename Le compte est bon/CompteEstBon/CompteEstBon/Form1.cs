using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompteEstBon
{
    public partial class Form1 : Form
    {
        private System.Windows.Forms.TextBox []txtPlq;
        public bool Stop = false;
        public Form1()
        {

            InitializeComponent();
            CreerPlaquettes();
            AffichePlaquettes(6);
        }

        // Les 10 plaquettes sont cr��es une fois pour toute
        void CreerPlaquettes()
        {
            txtPlq = new TextBox[10];
            for (int i = 0; i < 10; i++) {
                this.txtPlq[i] = new TextBox();
                // this.txtPlq1.Location = new System.Drawing.Point(3, 3);
                this.txtPlq[i].Name = "txtPlq"+ i;
                this.txtPlq[i].Size = new System.Drawing.Size(48, 20);
                this.txtPlq[i].TabIndex = i;
            }
        }
        /// <summary>
        /// Insertion dynamique de TextBox dans le container PanelPlaquettes en fonction du nombre de plaques choisies
        /// </summary>
        /// <param name="nb">nbre de plaquettes</param>
        private void AffichePlaquettes(int nb)
        {   // SuspendLayout et PerformLayout permettent de controler la logique de presentation
            // On applique la logique qu'apr�s avoir inser� tous les controles dans le container
            this.PanelPlaquettes.SuspendLayout();   
            this.PanelPlaquettes.Controls.Clear();  // Enleve toutes les anciennes plaquettes du container 
            for (int i = 0; i < nb; i++)
               this.PanelPlaquettes.Controls.Add(txtPlq[i]);
   
            this.PanelPlaquettes.ResumeLayout(false);
            this.PanelPlaquettes.PerformLayout();
        }


        // Decremente le nbr de plaquettes
        private void btnMoins_Click(object sender, EventArgs e)
        {
            int a = int.Parse(lblPlaquettes.Text);
            if (a > 2) a--;
            lblPlaquettes.Text = a.ToString();
            AffichePlaquettes(a);
        }

        // Incremente le nbr de plaquettes
        private void btnPlus_Click(object sender, EventArgs e)
        {
            int a = int.Parse(lblPlaquettes.Text);
            if (a < 10) a++;
            lblPlaquettes.Text = a.ToString();
            AffichePlaquettes(a);

        }

        private void btnAuRevoir_Click(object sender, EventArgs e)
        {
            MainScreen mSc = new MainScreen();
            mSc.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        
        }

        // Tranfere les plaquettes dans plq et le chiffre � trouver � la classe calcul.
        private void btnCalcul_Click(object sender, EventArgs e)
        {
            int nbPlaq=int.Parse(lblPlaquettes.Text);   // on est sur que c'est un int
            int[] plq = new int[nbPlaq];
            int chiffre;
            int result;
            bool ok = int.TryParse(txtChiffre.Text, out chiffre);
            if (!ok || chiffre <= 0)
            {
                MessageBox.Show(string.Format("\"{0}\" n'est pas un entier valide !", txtChiffre.Text));
                txtChiffre.Focus();
                return;
            }
            for (int i = 0; i < nbPlaq; i++) {
                ok = int.TryParse(txtPlq[i].Text, out result);
                if (!ok || result<=0) {
                    MessageBox.Show(string.Format("\"{0}\" n'est pas un entier valide!", txtPlq[i].Text));
                    txtPlq[i].Focus();
                    return;
                }
                plq[i] = result;
                if(result==chiffre){
                    MessageBox.Show(string.Format("\"{0}\" Resultat sur une plaquette !",chiffre));
                    txtPlq[i].Focus();
                    return;
                }

            }
            txtResultat.Text = "";
            Stop = false;
            Array.Sort<int>(plq);  // tri le tableau
            Array.Reverse(plq);    // par ordre decroissant ex : 100 75 10 10 4 3
            Calcul.Cherche(plq, chiffre,this);  // Le calcul
        }
    

   
   
        // Ce bouton met ma valeur Stop � true qui pourra �tre lue par le thread effectuant le calcul.
        private void btnStop_Click(object sender, EventArgs e)
        {
            Stop = true;
        }
    }
}