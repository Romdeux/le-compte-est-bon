﻿namespace CompteEstBon
{
    partial class PlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerForm));
            this.C1 = new System.Windows.Forms.Label();
            this.C2 = new System.Windows.Forms.Label();
            this.C3 = new System.Windows.Forms.Label();
            this.C4 = new System.Windows.Forms.Label();
            this.C5 = new System.Windows.Forms.Label();
            this.C6 = new System.Windows.Forms.Label();
            this.NombreChercher = new System.Windows.Forms.Label();
            this.AfficheNB = new System.Windows.Forms.Label();
            this.retour = new System.Windows.Forms.Button();
            this.DispSomme = new System.Windows.Forms.Label();
            this.add1 = new System.Windows.Forms.TextBox();
            this.sous1 = new System.Windows.Forms.TextBox();
            this.mult1 = new System.Windows.Forms.TextBox();
            this.divs1 = new System.Windows.Forms.TextBox();
            this.add2 = new System.Windows.Forms.TextBox();
            this.sous2 = new System.Windows.Forms.TextBox();
            this.mult2 = new System.Windows.Forms.TextBox();
            this.divs2 = new System.Windows.Forms.TextBox();
            this.DispDiff = new System.Windows.Forms.Label();
            this.DispProd = new System.Windows.Forms.Label();
            this.DispQuo = new System.Windows.Forms.Label();
            this.addition = new System.Windows.Forms.Button();
            this.soustraction = new System.Windows.Forms.Button();
            this.multiplication = new System.Windows.Forms.Button();
            this.division = new System.Windows.Forms.Button();
            this.calculeAddition = new System.Windows.Forms.Button();
            this.calculeSous = new System.Windows.Forms.Button();
            this.calculeMult = new System.Windows.Forms.Button();
            this.CalculeDivis = new System.Windows.Forms.Button();
            this.Paneltext = new System.Windows.Forms.FlowLayoutPanel();
            this.rejouer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // C1
            // 
            this.C1.AutoSize = true;
            this.C1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C1.Location = new System.Drawing.Point(75, 31);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(64, 25);
            this.C1.TabIndex = 0;
            this.C1.Text = "label1";
            // 
            // C2
            // 
            this.C2.AutoSize = true;
            this.C2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C2.Location = new System.Drawing.Point(166, 31);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(64, 25);
            this.C2.TabIndex = 1;
            this.C2.Text = "label2";
            // 
            // C3
            // 
            this.C3.AutoSize = true;
            this.C3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C3.Location = new System.Drawing.Point(267, 31);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(64, 25);
            this.C3.TabIndex = 2;
            this.C3.Text = "label3";
            // 
            // C4
            // 
            this.C4.AutoSize = true;
            this.C4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C4.Location = new System.Drawing.Point(362, 31);
            this.C4.Name = "C4";
            this.C4.Size = new System.Drawing.Size(64, 25);
            this.C4.TabIndex = 3;
            this.C4.Text = "label4";
            // 
            // C5
            // 
            this.C5.AutoSize = true;
            this.C5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C5.Location = new System.Drawing.Point(460, 31);
            this.C5.Name = "C5";
            this.C5.Size = new System.Drawing.Size(64, 25);
            this.C5.TabIndex = 4;
            this.C5.Text = "label5";
            // 
            // C6
            // 
            this.C6.AutoSize = true;
            this.C6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C6.Location = new System.Drawing.Point(561, 31);
            this.C6.Name = "C6";
            this.C6.Size = new System.Drawing.Size(64, 25);
            this.C6.TabIndex = 5;
            this.C6.Text = "label6";
            // 
            // NombreChercher
            // 
            this.NombreChercher.AutoSize = true;
            this.NombreChercher.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreChercher.Location = new System.Drawing.Point(12, 93);
            this.NombreChercher.Name = "NombreChercher";
            this.NombreChercher.Size = new System.Drawing.Size(161, 25);
            this.NombreChercher.TabIndex = 6;
            this.NombreChercher.Text = "Chiffre à trouver :";
            // 
            // AfficheNB
            // 
            this.AfficheNB.AutoSize = true;
            this.AfficheNB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AfficheNB.Location = new System.Drawing.Point(179, 93);
            this.AfficheNB.Name = "AfficheNB";
            this.AfficheNB.Size = new System.Drawing.Size(64, 25);
            this.AfficheNB.TabIndex = 7;
            this.AfficheNB.Text = "label1";
            // 
            // retour
            // 
            this.retour.Location = new System.Drawing.Point(112, 366);
            this.retour.Name = "retour";
            this.retour.Size = new System.Drawing.Size(75, 23);
            this.retour.TabIndex = 12;
            this.retour.Text = "retour";
            this.retour.UseVisualStyleBackColor = true;
            this.retour.Click += new System.EventHandler(this.retour_Click);
            // 
            // DispSomme
            // 
            this.DispSomme.AutoSize = true;
            this.DispSomme.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DispSomme.Location = new System.Drawing.Point(362, 184);
            this.DispSomme.Name = "DispSomme";
            this.DispSomme.Size = new System.Drawing.Size(0, 25);
            this.DispSomme.TabIndex = 13;
            // 
            // add1
            // 
            this.add1.Location = new System.Drawing.Point(17, 184);
            this.add1.Name = "add1";
            this.add1.Size = new System.Drawing.Size(100, 20);
            this.add1.TabIndex = 14;
            // 
            // sous1
            // 
            this.sous1.Location = new System.Drawing.Point(17, 223);
            this.sous1.Name = "sous1";
            this.sous1.Size = new System.Drawing.Size(100, 20);
            this.sous1.TabIndex = 15;
            // 
            // mult1
            // 
            this.mult1.Location = new System.Drawing.Point(17, 262);
            this.mult1.Name = "mult1";
            this.mult1.Size = new System.Drawing.Size(100, 20);
            this.mult1.TabIndex = 16;
            // 
            // divs1
            // 
            this.divs1.Location = new System.Drawing.Point(17, 301);
            this.divs1.Name = "divs1";
            this.divs1.Size = new System.Drawing.Size(100, 20);
            this.divs1.TabIndex = 17;
            // 
            // add2
            // 
            this.add2.Location = new System.Drawing.Point(184, 184);
            this.add2.Name = "add2";
            this.add2.Size = new System.Drawing.Size(100, 20);
            this.add2.TabIndex = 19;
            // 
            // sous2
            // 
            this.sous2.Location = new System.Drawing.Point(184, 223);
            this.sous2.Name = "sous2";
            this.sous2.Size = new System.Drawing.Size(100, 20);
            this.sous2.TabIndex = 20;
            // 
            // mult2
            // 
            this.mult2.Location = new System.Drawing.Point(184, 262);
            this.mult2.Name = "mult2";
            this.mult2.Size = new System.Drawing.Size(100, 20);
            this.mult2.TabIndex = 21;
            // 
            // divs2
            // 
            this.divs2.Location = new System.Drawing.Point(184, 301);
            this.divs2.Name = "divs2";
            this.divs2.Size = new System.Drawing.Size(100, 20);
            this.divs2.TabIndex = 22;
            // 
            // DispDiff
            // 
            this.DispDiff.AutoSize = true;
            this.DispDiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DispDiff.Location = new System.Drawing.Point(362, 223);
            this.DispDiff.Name = "DispDiff";
            this.DispDiff.Size = new System.Drawing.Size(0, 25);
            this.DispDiff.TabIndex = 24;
            // 
            // DispProd
            // 
            this.DispProd.AutoSize = true;
            this.DispProd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DispProd.Location = new System.Drawing.Point(362, 262);
            this.DispProd.Name = "DispProd";
            this.DispProd.Size = new System.Drawing.Size(0, 25);
            this.DispProd.TabIndex = 25;
            // 
            // DispQuo
            // 
            this.DispQuo.AutoSize = true;
            this.DispQuo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DispQuo.Location = new System.Drawing.Point(362, 297);
            this.DispQuo.Name = "DispQuo";
            this.DispQuo.Size = new System.Drawing.Size(0, 25);
            this.DispQuo.TabIndex = 26;
            // 
            // addition
            // 
            this.addition.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.addition.Location = new System.Drawing.Point(123, 182);
            this.addition.Name = "addition";
            this.addition.Size = new System.Drawing.Size(50, 23);
            this.addition.TabIndex = 27;
            this.addition.Text = "+";
            this.addition.UseVisualStyleBackColor = true;
            // 
            // soustraction
            // 
            this.soustraction.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.soustraction.Location = new System.Drawing.Point(123, 223);
            this.soustraction.Name = "soustraction";
            this.soustraction.Size = new System.Drawing.Size(50, 23);
            this.soustraction.TabIndex = 28;
            this.soustraction.Text = "-";
            this.soustraction.UseVisualStyleBackColor = true;
            // 
            // multiplication
            // 
            this.multiplication.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.multiplication.Location = new System.Drawing.Point(123, 262);
            this.multiplication.Name = "multiplication";
            this.multiplication.Size = new System.Drawing.Size(50, 23);
            this.multiplication.TabIndex = 29;
            this.multiplication.Text = "x";
            this.multiplication.UseVisualStyleBackColor = true;
            // 
            // division
            // 
            this.division.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.division.Location = new System.Drawing.Point(123, 301);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(50, 23);
            this.division.TabIndex = 30;
            this.division.Text = "/";
            this.division.UseVisualStyleBackColor = true;
            // 
            // calculeAddition
            // 
            this.calculeAddition.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.calculeAddition.Location = new System.Drawing.Point(299, 182);
            this.calculeAddition.Name = "calculeAddition";
            this.calculeAddition.Size = new System.Drawing.Size(50, 23);
            this.calculeAddition.TabIndex = 31;
            this.calculeAddition.Text = "calcule";
            this.calculeAddition.UseVisualStyleBackColor = true;
            this.calculeAddition.Click += new System.EventHandler(this.calculeAddition_Click);
            // 
            // calculeSous
            // 
            this.calculeSous.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.calculeSous.Location = new System.Drawing.Point(299, 220);
            this.calculeSous.Name = "calculeSous";
            this.calculeSous.Size = new System.Drawing.Size(50, 23);
            this.calculeSous.TabIndex = 32;
            this.calculeSous.Text = "calcule";
            this.calculeSous.UseVisualStyleBackColor = true;
            this.calculeSous.Click += new System.EventHandler(this.calculeSous_Click);
            // 
            // calculeMult
            // 
            this.calculeMult.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.calculeMult.Location = new System.Drawing.Point(299, 259);
            this.calculeMult.Name = "calculeMult";
            this.calculeMult.Size = new System.Drawing.Size(50, 23);
            this.calculeMult.TabIndex = 33;
            this.calculeMult.Text = "calcule";
            this.calculeMult.UseVisualStyleBackColor = true;
            this.calculeMult.Click += new System.EventHandler(this.calculeMult_Click);
            // 
            // CalculeDivis
            // 
            this.CalculeDivis.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.CalculeDivis.Location = new System.Drawing.Point(299, 298);
            this.CalculeDivis.Name = "CalculeDivis";
            this.CalculeDivis.Size = new System.Drawing.Size(50, 23);
            this.CalculeDivis.TabIndex = 34;
            this.CalculeDivis.Text = "calcule";
            this.CalculeDivis.UseVisualStyleBackColor = true;
            this.CalculeDivis.Click += new System.EventHandler(this.CalculeDivis_Click);
            // 
            // Paneltext
            // 
            this.Paneltext.Location = new System.Drawing.Point(594, 160);
            this.Paneltext.Name = "Paneltext";
            this.Paneltext.Size = new System.Drawing.Size(310, 173);
            this.Paneltext.TabIndex = 35;
            // 
            // rejouer
            // 
            this.rejouer.Location = new System.Drawing.Point(12, 366);
            this.rejouer.Name = "rejouer";
            this.rejouer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rejouer.Size = new System.Drawing.Size(75, 23);
            this.rejouer.TabIndex = 36;
            this.rejouer.Text = "Rejouer";
            this.rejouer.UseVisualStyleBackColor = true;
            this.rejouer.Click += new System.EventHandler(this.button1_Click);
            // 
            // PlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(916, 584);
            this.Controls.Add(this.rejouer);
            this.Controls.Add(this.Paneltext);
            this.Controls.Add(this.CalculeDivis);
            this.Controls.Add(this.calculeMult);
            this.Controls.Add(this.calculeSous);
            this.Controls.Add(this.calculeAddition);
            this.Controls.Add(this.division);
            this.Controls.Add(this.multiplication);
            this.Controls.Add(this.soustraction);
            this.Controls.Add(this.addition);
            this.Controls.Add(this.DispQuo);
            this.Controls.Add(this.DispProd);
            this.Controls.Add(this.DispDiff);
            this.Controls.Add(this.divs2);
            this.Controls.Add(this.mult2);
            this.Controls.Add(this.sous2);
            this.Controls.Add(this.add2);
            this.Controls.Add(this.divs1);
            this.Controls.Add(this.mult1);
            this.Controls.Add(this.sous1);
            this.Controls.Add(this.add1);
            this.Controls.Add(this.DispSomme);
            this.Controls.Add(this.retour);
            this.Controls.Add(this.AfficheNB);
            this.Controls.Add(this.NombreChercher);
            this.Controls.Add(this.C6);
            this.Controls.Add(this.C5);
            this.Controls.Add(this.C4);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.C1);
            this.Name = "PlayerForm";
            this.Text = "PlayerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label C1;
        private System.Windows.Forms.Label C2;
        private System.Windows.Forms.Label C3;
        private System.Windows.Forms.Label C4;
        private System.Windows.Forms.Label C5;
        private System.Windows.Forms.Label C6;
        private System.Windows.Forms.Label NombreChercher;
        private System.Windows.Forms.Label AfficheNB;
        private System.Windows.Forms.Button retour;
        private System.Windows.Forms.Label DispSomme;
        private System.Windows.Forms.TextBox add1;
        private System.Windows.Forms.TextBox sous1;
        private System.Windows.Forms.TextBox mult1;
        private System.Windows.Forms.TextBox divs1;
        private System.Windows.Forms.TextBox add2;
        private System.Windows.Forms.TextBox sous2;
        private System.Windows.Forms.TextBox mult2;
        private System.Windows.Forms.TextBox divs2;
        private System.Windows.Forms.Label DispDiff;
        private System.Windows.Forms.Label DispProd;
        private System.Windows.Forms.Label DispQuo;
        private System.Windows.Forms.Button addition;
        private System.Windows.Forms.Button soustraction;
        private System.Windows.Forms.Button multiplication;
        private System.Windows.Forms.Button division;
        private System.Windows.Forms.Button calculeAddition;
        private System.Windows.Forms.Button calculeSous;
        private System.Windows.Forms.Button calculeMult;
        private System.Windows.Forms.Button CalculeDivis;
        private System.Windows.Forms.FlowLayoutPanel Paneltext;
        private System.Windows.Forms.Button rejouer;
    }
}