﻿namespace CompteEstBon
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtChiffre = new System.Windows.Forms.TextBox();
            this.txtResultat = new System.Windows.Forms.TextBox();
            this.btnCalcul = new System.Windows.Forms.Button();
            this.btnAuRevoir = new System.Windows.Forms.Button();
            this.lblResInt = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.tipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.PanelPlaquettes = new System.Windows.Forms.FlowLayoutPanel();
            this.lblPlaquettes = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 350);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Chiffre :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Resultat :";
            // 
            // txtChiffre
            // 
            this.txtChiffre.Location = new System.Drawing.Point(15, 367);
            this.txtChiffre.Name = "txtChiffre";
            this.txtChiffre.Size = new System.Drawing.Size(100, 20);
            this.txtChiffre.TabIndex = 11;
            // 
            // txtResultat
            // 
            this.txtResultat.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultat.Location = new System.Drawing.Point(15, 88);
            this.txtResultat.MaxLength = 300000;
            this.txtResultat.Multiline = true;
            this.txtResultat.Name = "txtResultat";
            this.txtResultat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResultat.Size = new System.Drawing.Size(260, 187);
            this.txtResultat.TabIndex = 7;
            this.txtResultat.TabStop = false;
            // 
            // btnCalcul
            // 
            this.btnCalcul.Location = new System.Drawing.Point(15, 281);
            this.btnCalcul.Name = "btnCalcul";
            this.btnCalcul.Size = new System.Drawing.Size(75, 23);
            this.btnCalcul.TabIndex = 12;
            this.btnCalcul.Text = "Calcul";
            this.btnCalcul.UseVisualStyleBackColor = true;
            this.btnCalcul.Click += new System.EventHandler(this.btnCalcul_Click);
            // 
            // btnAuRevoir
            // 
            this.btnAuRevoir.Location = new System.Drawing.Point(189, 281);
            this.btnAuRevoir.Name = "btnAuRevoir";
            this.btnAuRevoir.Size = new System.Drawing.Size(86, 23);
            this.btnAuRevoir.TabIndex = 9;
            this.btnAuRevoir.TabStop = false;
            this.btnAuRevoir.Text = "Retour menu";
            this.btnAuRevoir.UseVisualStyleBackColor = true;
            this.btnAuRevoir.Click += new System.EventHandler(this.btnAuRevoir_Click);
            // 
            // lblResInt
            // 
            this.lblResInt.AutoSize = true;
            this.lblResInt.Location = new System.Drawing.Point(257, 69);
            this.lblResInt.MinimumSize = new System.Drawing.Size(10, 10);
            this.lblResInt.Name = "lblResInt";
            this.lblResInt.Size = new System.Drawing.Size(16, 13);
            this.lblResInt.TabIndex = 35;
            this.lblResInt.Text = " 0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(168, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Valeur proche :";
            this.tipInfo.SetToolTip(this.label1, "Solution la plus proche.");
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(102, 281);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 45;
            this.btnStop.Text = "STOP !";
            this.tipInfo.SetToolTip(this.btnStop, "Stoppe le calcul en cours");
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // tipInfo
            // 
            this.tipInfo.IsBalloon = true;
            this.tipInfo.ShowAlways = true;
            // 
            // PanelPlaquettes
            // 
            this.PanelPlaquettes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelPlaquettes.Location = new System.Drawing.Point(15, 393);
            this.PanelPlaquettes.Name = "PanelPlaquettes";
            this.PanelPlaquettes.Size = new System.Drawing.Size(712, 103);
            this.PanelPlaquettes.TabIndex = 0;
            // 
            // lblPlaquettes
            // 
            this.lblPlaquettes.AutoSize = true;
            this.lblPlaquettes.Location = new System.Drawing.Point(1, 394);
            this.lblPlaquettes.Name = "lblPlaquettes";
            this.lblPlaquettes.Size = new System.Drawing.Size(13, 13);
            this.lblPlaquettes.TabIndex = 3;
            this.lblPlaquettes.Text = "6";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(728, 496);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblResInt);
            this.Controls.Add(this.btnAuRevoir);
            this.Controls.Add(this.btnCalcul);
            this.Controls.Add(this.txtResultat);
            this.Controls.Add(this.txtChiffre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPlaquettes);
            this.Controls.Add(this.PanelPlaquettes);
            this.Name = "Form1";
            this.Text = "Compte est Bon";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtChiffre;
        public System.Windows.Forms.TextBox txtResultat;
        private System.Windows.Forms.Button btnCalcul;
        private System.Windows.Forms.Button btnAuRevoir;
        public System.Windows.Forms.Label lblResInt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip tipInfo;
        protected internal System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.FlowLayoutPanel PanelPlaquettes;
        private System.Windows.Forms.Label lblPlaquettes;
    }
}

