﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompteEstBon
{
    public partial class PlayerForm : Form
    {
        private int[] randNumbersBigNb = new int[2];
        private int[] randNumbersSmallNb = new int[4];
        Label S1 = new Label();
        Label S2 = new Label();
        Label S3 = new Label();
        Label S4 = new Label();

        public PlayerForm()
        {
            InitializeComponent();
        }

        private void retour_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainScreen mSc = new MainScreen();
            mSc.Show();
        }

        private void addition_Click(object sender, EventArgs e)
        {

        }

        public void RandNumber ()
        {
            Random rnd = new Random();
            for (int i = 0; i < 2; i++)
            {
                randNumbersBigNb[i] = rnd.Next(1, 100);
            }

            for (int j = 0; j < 4; j++)
            {
                randNumbersSmallNb[j] = rnd.Next(1, 10);
            }

            C1.Text = randNumbersBigNb[0].ToString();
            C2.Text = randNumbersSmallNb[0].ToString();
            C3.Text = randNumbersSmallNb[1].ToString();
            C4.Text = randNumbersBigNb[1].ToString();
            C5.Text = randNumbersSmallNb[2].ToString();
            C6.Text = randNumbersSmallNb[3].ToString();
            AfficheNB.Text = rnd.Next(201, 999).ToString();
        }

        public void Calcul()
        {

        }

        private void calculeAddition_Click(object sender, EventArgs e)
        {
            int somme = int.Parse(add1.Text) + int.Parse(add2.Text);
            DispSomme.Text = somme.ToString();
            add1.Text = "";
            add2.Text = "";
            S1.Name = "S1";
            S1.Text = somme.ToString();
            Paneltext.Controls.Add(S1);
        }

        private void calculeSous_Click(object sender, EventArgs e)
        {
            int difference = int.Parse(sous1.Text) - int.Parse(sous2.Text);
            DispDiff.Text = difference.ToString();
            sous1.Text = "";
            sous2.Text = "";
            S2.Name = "S2";
            S2.Text = difference.ToString();
            Paneltext.Controls.Add(S2);
        }

        private void calculeMult_Click(object sender, EventArgs e)
        {
            int produit = int.Parse(mult1.Text) * int.Parse(mult2.Text);
            DispProd.Text = produit.ToString();
            mult1.Text = "";
            mult2.Text = "";
            S3.Name = "S1";
            S3.Text = produit.ToString();
            Paneltext.Controls.Add(S3);
        }

        private void CalculeDivis_Click(object sender, EventArgs e)
        {
            if(divs1.Text != "0" && divs2.Text != "0")
            {
                int quotien = int.Parse(divs1.Text) / int.Parse(divs2.Text);
                if(int.Parse(divs1.Text) % int.Parse(divs2.Text) == 0)
                {
                    DispQuo.Text = quotien.ToString();
                    divs1.Text = "";
                    divs2.Text = "";                    
                    S4.Name = "S4";
                    S4.Text = quotien.ToString();
                    Paneltext.Controls.Add(S4);
                }                
            }            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            S1.Text = "";
            S2.Text = "";
            S3.Text = "";
            S4.Text = "";
            DispDiff.Text = "";
            DispProd.Text = "";
            DispSomme.Text = "";
            DispQuo.Text = "";
            RandNumber();
        }
    }
}
